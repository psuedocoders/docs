//
//  ViewController.h
//  iOSParentAppPrototype
//
//  Created by Faisal Alshemaimry on 6/07/2015.
//  Copyright (c) 2015 schoolsmart. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationFinder.h"
#import "UIApplication+SimulatorRemoteNotifications.h"

@interface ViewController : UIViewController <LocationFinderDelegate>
@property (strong, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (strong, nonatomic) IBOutlet UILabel *latitudeLabel;
-(void)locationFound:(CLLocation *)location;
@end

