//
//  LocationFinder.h
//  iOSParentAppPrototype
//
//  A utility class that finds the coordinates of the user and
//  passes them to its delegate.
//
//  Created by Faisal Alshemaimry on 6/07/2015.
//  Copyright (c) 2015 schoolsmart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationFinderDelegate
-(void) locationFound:(CLLocation*) location;
@end

@interface LocationFinder : NSObject <CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, assign) id delegate;
-(void) findLocation;
@end
