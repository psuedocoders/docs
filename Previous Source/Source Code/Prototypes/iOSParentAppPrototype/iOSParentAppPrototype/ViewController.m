//
//  ViewController.m
//  iOSParentAppPrototype
//
//  Created by Faisal Alshemaimry on 6/07/2015.
//  Copyright (c) 2015 schoolsmart. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.longitudeLabel.text = @"";
    self.latitudeLabel.text = @"";
    
    LocationFinder* locationFinder = [[LocationFinder alloc] init];
    locationFinder.delegate = self;
    [locationFinder findLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)locationFound:(CLLocation *)location {
    self.longitudeLabel.text = [[NSString alloc] initWithFormat:@"%+.6f", location.coordinate.longitude];
    self.latitudeLabel.text = [[NSString alloc] initWithFormat:@"%+.6f", location.coordinate.latitude];
}

@end
