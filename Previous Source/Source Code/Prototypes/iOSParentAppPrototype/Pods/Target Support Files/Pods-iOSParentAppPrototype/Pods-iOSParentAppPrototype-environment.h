
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// SimulatorRemoteNotifications
#define COCOAPODS_POD_AVAILABLE_SimulatorRemoteNotifications
#define COCOAPODS_VERSION_MAJOR_SimulatorRemoteNotifications 0
#define COCOAPODS_VERSION_MINOR_SimulatorRemoteNotifications 0
#define COCOAPODS_VERSION_PATCH_SimulatorRemoteNotifications 3

// Unirest
#define COCOAPODS_POD_AVAILABLE_Unirest
#define COCOAPODS_VERSION_MAJOR_Unirest 1
#define COCOAPODS_VERSION_MINOR_Unirest 1
#define COCOAPODS_VERSION_PATCH_Unirest 4

