﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397705
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
module GoogleMapsPrototype {
    "use strict";

    export module Application {
        export function initialize() {
            document.addEventListener('deviceready', onDeviceReady, false);
        }

        var map: google.maps.Map;
        var bermudaTriangle: google.maps.Polygon;

        function initGoogleMaps() {
            var mapOptions = {
                zoom: 5,
                center: new google.maps.LatLng(24.886436490787712, -70.2685546875),
                mapTypeId: google.maps.MapTypeId.TERRAIN
            };

            map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);

            // Define the LatLng coordinates for the polygon's path.
            var triangleCoords = [
                new google.maps.LatLng(25.774252, -80.190262),
                new google.maps.LatLng(18.466465, -66.118292),
                new google.maps.LatLng(32.321384, -64.75737),
                new google.maps.LatLng(25.774252, -80.190262)
            ];

            // Construct the polygon.
            bermudaTriangle = new google.maps.Polygon({
                paths: triangleCoords,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.00,
                visible: false
            });

            bermudaTriangle.setMap(map);
        }


        function onDeviceReady() {
            // Handle the Cordova pause and resume events
            document.addEventListener('pause', onPause, false);
            document.addEventListener('resume', onResume, false);

            initGoogleMaps();

            google.maps.event.addListener(map, 'click', e => {
                var result;
                if (google.maps.geometry.poly.containsLocation(e.latLng, bermudaTriangle)) {
                    result = 'red';
                } else {
                    result = 'green';
                }

                var circle = {
                    path: google.maps.SymbolPath.CIRCLE,
                    fillColor: result,
                    fillOpacity: .2,
                    strokeColor: 'white',
                    strokeWeight: .5,
                    scale: 10
                };
                var marker = new google.maps.Marker({
                    position: e.latLng,
                    map: map,
                    icon: circle
                });
            });
        }

        function onPause() {
            // TODO: This application has been suspended. Save application state here.
        }

        function onResume() {
            // TODO: This application has been reactivated. Restore application state here.
        }

    }

    window.onload = function () {
        Application.initialize();
    }
}
