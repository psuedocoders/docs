import time
import requests

class KioskConsole:
    """
    Console dummy for kiosk proof of concept.

    written by Jesse Todisco for Team SALTy final year project

    The final version of the console would incorporate a GUI with this as a controller driving the GUI.
    The model is handled by the server backend. This kiosk even when complete has minimal responsibility.
    The logic of this console is pending change as decisions are made regarding more design specifics,
    namely regarding what is the responsibilities of the server and kiosk.

    to initiate a server call, use the following command after running the console:
    send <studentid> <kioskid> <response>
    eg: send 12345678 1 1

    if you want to send no response (ie: initiating first touch on with the server)
    send 12345678 1 <space here>
    alternatively use 0 for the response type
    send 12345678 1 0
    """

    def message_loop(self):
        """
        Main loop of the kiosk module.
        Polls the user for input
        :return: None
        """

        message = ""

        # basic message loop, exit will close the program
        # if the user enters exit, the program closes
        # if the user enters a valid send command it contacts the server
        # if the user enters anything else it tells it how to enter a valid command

        while not message == "exit":
            message = input("> ")
            if message == "exit":
                break
            start_time = time.time()
            self.message_handler(message)
            print("\n\tProcess completed in %s ms\n" % ((time.time() - start_time) * 1000))

    def message_handler(self, message):
        """
        Sends request message to the server via HTTP
        :param message: string of arguments to message to the server
        :return: None
        """
        # splits the input message into its separate parts
        # the studentid and response are picked and sent to the server as arguments
        message = message.split(" ")
        # this message doesn't do anything unless it has the three valid parts (see above)
        if len(message) == 4 and message[0] == "send":
            # handling null response as 0
            if message[3] == "0":
                message[3] = ""

            # sending a HTTP get request to the server
            url = "http://vmh18197.hosting24.com.au/api/Student/PostStudentNotification/"
            data = dict(rfid=message[1], kioskId=message[2], response=message[3])
            # we post the information using the requests package, returning the server response
            # for us to use
            r = requests.post(url, data)

            # the variable we get, r, contains the response that we can pick apart as json data
            self.display_json_message(r.json())

        else:
            # if the command is incorrect, tell the user how to do the right thing
            print("\nIncorrect command. Enter \'send studentid kioskid responseno\'")
            print("\nAlternatively, type \'exit\' to quit the program.")

    @staticmethod
    def display_json_message(message):
        """
        Decodes a json message into the parts relevant to the kiosk display
        :param message: a json message containing relevant kiosk data
        :return: always True
        """
        # we're going to check the message response to see that we got the right details
        success_fields = {'Tone', 'Message', 'ResponseRequired', 'ButtonOneText', 'ButtonTwoText', 'ButtonThreeText'}
        generic_failure_fields = {'Message', 'Success', 'Exception', 'Stack'}
        exception_fields = {'ExceptionMessage', 'StackTrace'}

        # if it matches the fields we want, we want to dissect it
        if success_fields.issubset(message):

            # default return message when there is no response
            print("Tone: " + str(message['Tone']) + "\n" +
                  "Message: " + message['Message'] + "\n\n")

            # if the server requires a response in the next message, show the options and request a response
            if message['ResponseRequired'] == "True":
                print("1: " + message['ButtonOneText'] + "\n" +
                      "2: " + message['ButtonTwoText'] + "\n" +
                      "3: " + message['ButtonThreeText'] + "\n" +
                      "To reply, enter the same studentId and supply the last argument as 1/2/3 \n\n")

        # if we can't parse the message we should display what it is for debug purposes
        elif exception_fields.issubset(message):
            print("Error has been caught")
            print(message['ExceptionMessage'] + "\n")
            print("Stack Trace:")
            print(message['StackTrace'])
        elif generic_failure_fields.issubset(message):
            print("Oh no! Something happened!")
            print("Message: " + message['Message'])
            print("Success: " + str(message['Success']))
            print("Exception: " + str(message['Exception']))
            print("Stack: " + str(message['Stack']))
        else:
            # if it's not even an exception we just print the result
            print("Oh no! Something happened!")
            print(message)

        return True


if '__main__' == __name__:
    kiosk = KioskConsole()
    kiosk.message_loop()

