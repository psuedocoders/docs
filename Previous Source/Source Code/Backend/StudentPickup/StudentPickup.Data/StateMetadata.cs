﻿using System.ComponentModel.DataAnnotations;

namespace StudentPickup.Data
{
    [MetadataType(typeof(StateMetadata))]
    public partial class State
    {
    }

    public class StateMetadata
    {
        [Display(Name = "State Id")]
        public int StateId { get; set; }
        [Display(Name = "State Name")]
        public string Name { get; set; }
        [Display(Name = "Country Id")]
        public int CountryId { get; set; }
    }
}
