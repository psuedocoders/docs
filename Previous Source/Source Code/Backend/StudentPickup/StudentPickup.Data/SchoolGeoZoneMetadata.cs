﻿using System.ComponentModel.DataAnnotations;

namespace StudentPickup.Data
{
    [MetadataType(typeof(SchoolGeoZoneMetadata))]
    public partial class SchoolGeoZone
    {
    }

    public class SchoolGeoZoneMetadata
    {
        [Display(Name = "School Geo Zone Id")]
        public int SchoolGeoZoneId { get; set; }
        [Display(Name = "School Geo Zone Name")]
        public string Name { get; set; }
        [Display(Name = "School Id")]
        public int SchoolId { get; set; }
        [Display(Name = "Geo Zone Type Id")]
        public int GeoZoneTypeId { get; set; }
        [Display(Name = "Guardian Message")]
        public int GuardianMessage { get; set; }
        [Display(Name = "Description")]
        public int Description { get; set; }
    }
}
