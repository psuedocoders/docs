﻿using System.ComponentModel.DataAnnotations;

namespace StudentPickup.Data
{
    [MetadataType(typeof(GeoZoneTypeMetadata))]
    public partial class GeoZoneType
    {
    }

    public class GeoZoneTypeMetadata
    {
        [Display(Name = "Geo Zone Type Id")]
        public int GeoZoneTypeId { get; set; }
        [Display(Name = "Geo Zone Type Name")]
        public string Name { get; set; }
        [Display(Name = "Description")]
        public int Description { get; set; }
    }
}
