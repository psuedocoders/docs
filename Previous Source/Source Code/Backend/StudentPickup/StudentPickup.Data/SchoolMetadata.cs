﻿using System.ComponentModel.DataAnnotations;

namespace StudentPickup.Data
{
    [MetadataType(typeof(SchoolMetadata))]
    public partial class School
    {
    }

    public class SchoolMetadata
    {
        [Display(Name="School Id")]
        public int SchoolId { get; set; }
        [Display(Name = "School Name")]
        public string Name { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "Contact Name")]
        public string ContactName { get; set; }
        [Display(Name = "Contact Email")]
        public string ContactEmail { get; set; }
        [Display(Name = "Contact Number")]
        public string ContactNumber { get; set; }
        [Display(Name = "State Id")]
        public int StateId { get; set; }
    }
}
