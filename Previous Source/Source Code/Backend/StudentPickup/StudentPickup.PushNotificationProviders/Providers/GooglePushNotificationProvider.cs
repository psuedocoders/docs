﻿using System;
using System.Configuration;
using PushSharp;
using PushSharp.Android;
using PushSharp.Core;
using StudentPickup.PushNotificationProviders.Interfaces;

namespace StudentPickup.PushNotificationProviders.Providers
{
    public class GooglePushNotificationService : IPushNotificationService
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void SendNotification(string deviceId, string message)
        {
            //Create our push services broker
            var push = new PushBroker();

            //Wire up the events for all the services that the broker registers
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.OnChannelCreated += ChannelCreated;
            push.OnChannelDestroyed += ChannelDestroyed;


            push.RegisterGcmService(new GcmPushChannelSettings(ConfigurationManager.AppSettings["GCMApiKey"]));

            push.QueueNotification(new GcmNotification().ForDeviceRegistrationId(deviceId)
                                  .WithJson(string.Format(@"{{""alert"":""{0}""}}", message)));


            //Stop and wait for the queues to drains
            push.StopAllServices();
        }

        private void ChannelDestroyed(object sender)
        {
            Log.Info("Channel Destroyed");
        }

        private void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            Log.Info("Channel Created");
        }

        private void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            Log.Info(String.Format("Device Subscription Changed, oldId: {0}, newId: {1}", oldSubscriptionId, newSubscriptionId));
        }

        private void DeviceSubscriptionExpired(object sender, string expiredSubscriptionId, DateTime expirationDateUtc, INotification notification)
        {
            Log.Info(String.Format("Device Subscription Expired, expiredSubId: {0}, expiredDate: {1}", expiredSubscriptionId, expirationDateUtc));
        }

        private void NotificationFailed(object sender, INotification notification, Exception error)
        {
            Log.Error("Notification Failed", error);
        }

        private void ServiceException(object sender, Exception error)
        {
            Log.Error("Service Exception", error);
        }

        private void ChannelException(object sender, IPushChannel pushChannel, Exception error)
        {
            Log.Error("Channel Exception", error);
        }

        private void NotificationSent(object sender, INotification notification)
        {
            Log.Info("Notification Sent!");
        }
    }
}
