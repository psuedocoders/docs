﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;

namespace StudentPickup.Service
{
    public class StudentService : Service<Student>, IStudentService
    {
        private readonly IRepositoryAsync<Student> _repository;

        public StudentService(IRepositoryAsync<Student> repository)
            : base(repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the extra students that will be picked up in the carpool by the students guardian today. Returns an empty list if no other students are being picked up.
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian</param>
        /// <param name="schoolId">The unique identifier of the school</param>
        /// <returns>The students that will be picked up by the guardian today. Empty list if no one other than the student is being picked up.</returns>
        public async Task<List<Student>> GetCarpoolParticipantsByGuardianToday(int guardianId, int schoolId)
        {
            return await _repository.GetCarpoolParticipantsByGuardianToday(guardianId, schoolId);
        }

        /// <summary>
        /// Determines whether the student is being picked up today.
        /// </summary>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <returns>Whether the student is being picked up today.</returns>
        public async Task<bool> StudentBeingPickedUpToday(int studentId)
        {
            return await _repository.StudentBeingPickedUpToday(studentId);
        }

        /// <summary>
        /// Determines whether the student is part of a carpool today for the guardian at the particular school.
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian</param>
        /// <param name="schoolId">The unique identifier of the school</param>
        /// <returns>Whether the student is in a carpool.</returns>
        public async Task<bool> IsStudentInCarpoolToday(int guardianId, int schoolId)
        {
            var guardianCarpool = await GetCarpoolParticipantsByGuardianToday(guardianId, schoolId);
            return guardianCarpool.Count > 1;
        }

        /// <summary>
        /// Gets a student by their rfid.
        /// </summary>
        /// <param name="rfid">The rfid of the student to find.</param>
        /// <returns>The student with the rfid.</returns>
        public async Task<Student> GetStudentByRfid(string rfid)
        {
            return await _repository.GetStudentByRfid(rfid);
        }
    }
}
