﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentPickup.Core.PushNotifications;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    /// <summary>
    /// Not to be confused with the services for the repositories, this just provides various methods for push notifications
    /// </summary>
    public interface IPushNotificationService
    {
        /// <summary>
        /// Updates the guardians last known location
        /// </summary>
        /// <param name="guardianId">The unique identifier of the Guardian to update</param>
        /// <param name="studentName">Name of the guardians student</param>
        /// <returns>The guardian with the updated location</returns>
        Task<bool> SendDeviceNotifications(int guardianId, string studentName);

        /// <summary>
        /// Gets the guardian by their id if the date of their Last Recored Location Timestamp is today
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian to search for</param>
        /// <param name="inValidZone">Is the guardian in a valid geo zone</param>
        /// <param name="studentName">Name of the guardians student</param>
        /// <returns>The object of the guardian if their location has been updated today, 
        /// Null if the guardian with that id hasn't had their location updated today.</returns>
        Task<bool> SendGeozoneNotification(int guardianId, bool inValidZone, string studentName);

        /// <summary>
        /// Get the guardian scheduled to pickup the student today
        /// </summary>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <param name="studentName">Name of the guardians student</param>
        /// <returns>The guardian scheduled to pickup the student today.</returns>
        Task<bool> SendTrustedDevicesNotifications(int studentId, string studentName);
    }

    public class PushNotificationService : IPushNotificationService
    {
        private readonly IGuardianDeviceService _guardianDeviceService;
        private readonly IGuardianService _guardianService;
        private readonly IPushNotificationProvider _pushNotificationProvider;

        public PushNotificationService(IGuardianDeviceService guardianDeviceService, IGuardianService guardianService, IPushNotificationProvider pushNotificationProvider)
        {
            _guardianDeviceService = guardianDeviceService;
            _guardianService = guardianService;
            _pushNotificationProvider = pushNotificationProvider;
        }

        /// <summary>
        /// Sends out push notifications to guardian devices.
        /// </summary>
        /// <param name="guardianId">Unique identifier for guardian</param>
        /// <param name="studentName">Student's name</param>
        /// <returns>TBC</returns>
        public async Task<bool> SendDeviceNotifications(int guardianId, string studentName)
        {
            var successfullySentNotification = true;
            var devices = await _guardianDeviceService.GetScheduledGuardianDeviceByGuardian(guardianId);
            foreach (var guardianDevice in devices)
            {
                // TODO: Need to integrate with circles to send appropriate message
                // find first instance of provider that has a matching Device Operating System Id to the current guardian device
                var pushNotificationAdapter = _pushNotificationProvider.GetPushNotificationAdapter((int)guardianDevice.DeviceOperatingSystemId);
                var notificationSent = pushNotificationAdapter.SendPushNotification(guardianDevice.DeviceAppId, PushNotificationTypes.LocationRequest,
                    $"{studentName} is ready for pickup");
                // Only return false if isn't already false and current push notification failed
                if (successfullySentNotification && !notificationSent)
                    successfullySentNotification = false;
            }
            return successfullySentNotification;
        }
        
        /// <summary>
        /// Sends out push notifications to guardian devices depending on geozone.
        /// </summary>
        /// <param name="guardianId">Unique identifier for guardian</param>
        /// <param name="inValidZone">Is the guardian in a valid geo zone</param>
        /// <param name="studentName">Name of the guardians student</param>
        /// <returns>TBC</returns>
        public async Task<bool> SendGeozoneNotification(int guardianId, bool inValidZone, string studentName)
        {
            string message;
            var customData = new Dictionary<string, string>();
            if (inValidZone)
            {
                customData.Add("Color", "Green");
                //TODO have path for tone or whatever value is needed instead of "good"
                customData.Add("Tone", "Good");
                message = $"You are in a valid zone, you may continue on to picking up {studentName}.";
            }
            else
            {
                message = $"You are in an invalid geozone, your child will be told to try again until you are in a valid zone";
                customData.Add("Color", "Red");
                //TODO have path for tone or whatever value is needed instead of "bad"
                customData.Add("Tone", "Bad");
            }

            var successfullySentNotification = true;
            var devices = await _guardianDeviceService.GetScheduledGuardianDeviceByGuardian(guardianId);
            foreach (var guardianDevice in devices)
            {
                // find first instance of provider that has a matching Device Operating System Id to the current guardian device
                var pushNotificationAdapter = _pushNotificationProvider.GetPushNotificationAdapter((int)guardianDevice.DeviceOperatingSystemId);
                var notificationSent = pushNotificationAdapter.SendPushNotification(guardianDevice.DeviceAppId, PushNotificationTypes.GeozoneInformation,
                    message, customData);
                // Only return false if isn't already false and current push notification failed
                if (successfullySentNotification && !notificationSent)
                    successfullySentNotification = false;
            }
            return successfullySentNotification;
        }
        
        /// <summary>
        /// Sends out push notifications to trusted guardian devices when scheduled guardian is unreachable.
        /// </summary>
        /// <param name="studentId">Unique identifier for student</param>
        /// <param name="studentName">Student's name</param>
        /// <returns>TBC</returns>
        public async Task<bool> SendTrustedDevicesNotifications(int studentId, string studentName)
        {
            var successfullySentNotification = true;
            //get guardians in trusted circle
            var guardians = await _guardianService.GetTrustedGuardiansByStudentId(studentId);
            if (guardians.Any())
            {
                var devices = new List<GuardianDevice>();

                //add devices from all guardians to list
                foreach (var guardian in guardians)
                {
                    devices.AddRange(await _guardianDeviceService.GetScheduledGuardianDeviceByGuardian(guardian.GuardianId));
                }

                foreach (var guardianDevice in devices)
                {
                    // find first instance of provider that has a matching Device Operating System Id to the current guardian device
                    var pushNotificationAdapter = _pushNotificationProvider.GetPushNotificationAdapter((int)guardianDevice.DeviceOperatingSystemId);
                    var notificationSent = pushNotificationAdapter.SendPushNotification(guardianDevice.DeviceAppId, PushNotificationTypes.TrustedGuardianAlert,
                        $"Scheduled guardian cannot be contacted, would you like to pick up {studentName} instead?");
                    // Only return false if isn't already false and current push notification failed
                    if (successfullySentNotification && !notificationSent)
                        successfullySentNotification = false;
                }
            }
            else
            {
                successfullySentNotification = false;
            }

            return successfullySentNotification;
        }
    }
}
