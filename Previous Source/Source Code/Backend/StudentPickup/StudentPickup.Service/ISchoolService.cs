﻿using System.Threading.Tasks;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    /// <summary>
    /// The service to access the School table in the database
    /// </summary>
    public interface ISchoolService : IService<School>
    {
        /// <summary>
        /// Gets the school by the KioskId including the GeoZones and GeoZonePolygon Points
        /// </summary>
        /// <param name="kioskId">The unique identifier of the kiosk</param>
        /// <returns>The school with the GeoZones and GeoZonePolygon Points</returns>
        Task<School> GetSchoolIncludingGeoZonesByKiosk(int kioskId);
    }
}