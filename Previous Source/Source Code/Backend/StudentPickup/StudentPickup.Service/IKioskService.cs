﻿using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    /// <summary>
    /// The service to access the Kiosk table in the database
    /// </summary>
    public interface IKioskService : IService<Kiosk>
    {
    }
}