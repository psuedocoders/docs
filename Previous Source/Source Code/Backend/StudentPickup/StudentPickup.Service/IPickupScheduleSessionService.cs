﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    public interface IPickupScheduleSessionService : IService<PickupScheduleSession>
    {
        /// <summary>
        /// Gets the latest schedules of the guardian by Id including the students, schools and schools geo zone details
        /// </summary>
        /// <param name="guardianId">The unique identifier of the guardian to search for</param>
        /// <returns>Guardian with details</returns>
        Task<List<PickupScheduleSession>> GetLatestSchedulesByGuardianId(int guardianId);
    }
}