﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Core.PushNotifications;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;

namespace StudentPickup.Service
{
    public class GuardianDeviceService : Service<GuardianDevice>, IGuardianDeviceService
    {
        private readonly IRepositoryAsync<GuardianDevice> _repository;
        private readonly IPushNotificationProvider _pushNotificationProvider;

        public GuardianDeviceService(IRepositoryAsync<GuardianDevice> repository, IPushNotificationProvider pushNotificationProvider)
            : base(repository)
        {
            _repository = repository;
            _pushNotificationProvider = pushNotificationProvider;
        }

        /// <summary>
        /// Gets all devices for the guardian scheduled to pickup the student today
        /// </summary>
        /// <param name="guardianId">The id of the guardian to find the devices for</param>
        /// <returns>An enumeration of guardian devices</returns>
        public async Task<List<GuardianDevice>> GetScheduledGuardianDeviceByGuardian(int guardianId)
        {
            return await _repository.GetScheduledGuardianDeviceByGuardian(guardianId);
        }

        /// <summary>
        /// If the guardian doesn't already have a registered device, register one. If they do update the existing device
        /// with the new device app id and operating system details.
        /// </summary>
        /// <param name="guardianId">Guardian who owns the device</param>
        /// <param name="deviceAppId">The registered ID with the device push notification service</param>
        /// <param name="deviceOperatingSystemId">The operating system of the device</param>
        /// <returns>New or updated device for guardian</returns>
        public async Task<GuardianDevice> UpsertGuardianDevice(int guardianId, string deviceAppId, DeviceOperatingSystems deviceOperatingSystemId)
        {
            return await _repository.UpsertGuardianDevice(guardianId, deviceAppId, deviceOperatingSystemId);
        }
        
        /// <summary>
        /// Updates the device subscription id based on the old subscription id
        /// </summary>
        /// <param name="oldSubscriptionId">Previous device subscription id</param>
        /// <param name="newSubscriptionId">New device subscription id</param>
        /// <returns>Updated guardian device</returns>
        public async Task<GuardianDevice> UpdateGuardianDeviceSubscriptionId(string oldSubscriptionId, string newSubscriptionId)
        {
            return await _repository.UpdateGuardianDeviceSubscriptionId(oldSubscriptionId, newSubscriptionId);
        }

        /// <summary>
        /// Updates the device subscription id based on the old subscription id
        /// </summary>
        /// <param name="subscriptionId">Previous device subscription id</param>
        /// <returns>Updated guardian device</returns>
        public async Task<GuardianDevice> RemoveGuardianDeviceBySubscriptionId(string subscriptionId)
        {
            return await _repository.RemoveGuardianDeviceBySubscriptionId(subscriptionId);
        }

        /// <summary>
        /// Sends out push notifications to guardian devices to ask the device for it's location.
        /// </summary>
        /// <param name="guardianId">Unique identifier for guardian</param>
        /// <param name="studentName">Student's name</param>
        /// <returns>Where or not the notification was sent successfully</returns>
        public async Task<bool> SendDeviceLocationRequestNotifications(int guardianId, string studentName)
        {
            var successfullySentNotification = true;
            var devices = await GetScheduledGuardianDeviceByGuardian(guardianId);
            foreach (var guardianDevice in devices)
            {
                // find first instance of provider that has a matching Device Operating System Id to the current guardian device
                var pushNotificationAdapter = _pushNotificationProvider.GetPushNotificationAdapter((int)guardianDevice.DeviceOperatingSystemId);
                var notificationSent = pushNotificationAdapter.SendPushNotification(guardianDevice.DeviceAppId, PushNotificationTypes.LocationRequest,
                    $"{studentName} is ready for pickup");
                // Only return false if isn't already false and current push notification failed
                if (successfullySentNotification && !notificationSent)
                    successfullySentNotification = false;
            }
            return successfullySentNotification;
        }

        /// <summary>
        /// Sends out push notifications to guardian devices notifying them there student is ready for pickup or if they are in an invalid zone.
        /// </summary>
        /// <param name="guardianId">Unique identifier for guardian</param>
        /// <param name="guardianGeoZone">The geo zone the guardian is in</param>
        /// <param name="eta">Estimated time of arival in seconds guardian is from school</param>
        /// <param name="studentName">Name of the guardians student</param>
        /// <returns>Whether or not the notification was sent successfully</returns>
        public async Task<bool> SendGeozoneNotification(int guardianId, SchoolGeoZone guardianGeoZone, int eta, string studentName)
        {
            string message;
            var customData = new Dictionary<string, string>();
            var etaMinutes = Math.Round(Convert.ToDouble(eta) / 60);

            if (guardianGeoZone != null && (guardianGeoZone.GeoZoneTypeId == GeoZoneTypes.Red ||
                                            guardianGeoZone.GeoZoneTypeId == GeoZoneTypes.DeRatedRed))
            {

                message = @"You're in an no-park zone. Please move.";
                customData.Add("Color", "Red");
                customData.Add("Tone", "Bad");
            }
            else
            {
                if (etaMinutes <= 3 && (guardianGeoZone != null && guardianGeoZone.GeoZoneTypeId == GeoZoneTypes.Green))
                {
                    message = "Student(s) are ready for pickup, please proceed to pickup area.";
                    customData.Add("Color", "Green");
                    customData.Add("Tone", "Good");
                }
                else if (etaMinutes <= 15)
                {
                    message =
                        $"Student(s) ready but have been told you are {etaMinutes} minutes away and to try again then.";
                    customData.Add("Color", "Red");
                    customData.Add("Tone", "Bad");
                }
                else
                {
                    message =
                        "As you are still a fair way from school, student(s) have been told to go to after school care.";
                    customData.Add("Color", "Red");
                    customData.Add("Tone", "Bad");
                }
            }

            var successfullySentNotification = true;
            var devices = await GetScheduledGuardianDeviceByGuardian(guardianId);
            foreach (var guardianDevice in devices)
            {
                // find first instance of provider that has a matching Device Operating System Id to the current guardian device
                var pushNotificationAdapter = _pushNotificationProvider.GetPushNotificationAdapter((int)guardianDevice.DeviceOperatingSystemId);
                var notificationSent = pushNotificationAdapter.SendPushNotification(guardianDevice.DeviceAppId, PushNotificationTypes.GeozoneInformation,
                    message, customData);
                // Only return false if isn't already false and current push notification failed
                if (successfullySentNotification && !notificationSent)
                    successfullySentNotification = false;
            }
            return successfullySentNotification;
        }

        /// <summary>
        /// Sends out push notifications to trusted guardian devices when scheduled guardian is unreachable.
        /// </summary>
        /// <param name="studentName">Student's name</param>
        /// <param name="trustedGuardians"></param>
        /// <returns>Whether or not the push notifications were sent successfully</returns>
        public async Task<bool> SendTrustedDevicesNotifications(string studentName, List<Guardian> trustedGuardians)
        {
            var successfullySentNotification = true;
            if (trustedGuardians.Any())
            {
                var devices = new List<GuardianDevice>();

                //add devices from all guardians to list
                foreach (var guardian in trustedGuardians)
                {
                    devices.AddRange(await GetScheduledGuardianDeviceByGuardian(guardian.GuardianId));
                }

                foreach (var guardianDevice in devices)
                {
                    // find first instance of provider that has a matching Device Operating System Id to the current guardian device
                    var pushNotificationAdapter = _pushNotificationProvider.GetPushNotificationAdapter((int)guardianDevice.DeviceOperatingSystemId);
                    var notificationSent = pushNotificationAdapter.SendPushNotification(guardianDevice.DeviceAppId, PushNotificationTypes.TrustedGuardianAlert,
                        $"Scheduled guardian cannot be contacted, would you like to pick up {studentName} instead?");
                    // Only return false if isn't already false and current push notification failed
                    if (successfullySentNotification && !notificationSent)
                        successfullySentNotification = false;
                }
            }
            else
            {
                successfullySentNotification = false;
            }

            return successfullySentNotification;
        }
    }
}
