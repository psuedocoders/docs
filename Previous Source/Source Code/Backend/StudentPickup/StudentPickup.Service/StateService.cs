﻿using Repository.Pattern.Repositories;
using Service.Pattern;
using StudentPickup.Data;

namespace StudentPickup.Service
{
    public class StateService : Service<State>, IStateService
    {
        private readonly IRepositoryAsync<State> _repository;

        public StateService(IRepositoryAsync<State> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}
