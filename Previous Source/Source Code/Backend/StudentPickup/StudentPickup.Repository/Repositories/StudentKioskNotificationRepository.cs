﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using StudentPickup.Data;

namespace StudentPickup.Repository.Repositories
{
    /// <summary>
    /// The repository to access the Student Kiosk Notification table in the database
    /// </summary>
    public static class StudentKioskNotificationRepository
    {
        /// <summary>
        /// Gets the timestamp of the first student kiosk notification within the past hour. Returns the default DateTime if they have never made a transaction.
        /// </summary>
        /// <param name="repository">The repository to find the guardian student in</param>
        /// <param name="studentId">The unique identifier of the student to find</param>
        /// <returns>The timestamp of the students last notification</returns>
        public async static Task<DateTime?> GetFirstStudentTransactionTimestampInPastHour(this IRepositoryAsync<StudentKioskNotification> repository, int studentId)
        {
            var oneHourAgo = DateTime.UtcNow.AddHours(-1);
            var notification = await
                repository.Queryable()
                    .Where(s => s.StudentId == studentId && s.NotificationTimestamp > oneHourAgo)
                    .OrderBy(x => x.NotificationTimestamp)
                    .FirstOrDefaultAsync();

            return notification?.NotificationTimestamp;
        }


        /// <summary>
        /// Gets the timestamp of the last student kiosk notification. Returns the default DateTime if they have never made a transaction
        /// </summary>
        /// <param name="repository">The repository to find the guardian student in</param>
        /// <param name="studentId">The unique identifier of the student to find</param>
        /// <returns>The timestamp of the students last notification</returns>
        public async static Task<DateTime?> GetLastStudentTransactionTimestamp(this IRepositoryAsync<StudentKioskNotification> repository, int studentId)
        {
            var notification = await
                repository.Queryable()
                    .Where(s => s.StudentId == studentId)
                    .OrderByDescending(x => x.NotificationTimestamp)
                    .FirstOrDefaultAsync();

            return notification?.NotificationTimestamp;
        }

        /// <summary>
        /// Gets the timestamp of the last student kiosk notification for the kiosk. Returns the default DateTime if they have never made a transaction
        /// </summary>
        /// <param name="repository">The repository to find the guardian student in</param>
        /// <param name="studentId">The unique identifier of the student to find</param>
        /// <param name="kioskId">The unique identifier of the kiosk to find</param>
        /// <returns>The timestamp of the students last notification for the kiosk</returns>
        public async static Task<DateTime?> GetLastStudentTransactionTimestampByKiosk(this IRepositoryAsync<StudentKioskNotification> repository, int studentId, int kioskId)
        {

            var notification = await
                repository.Queryable()
                    .Where(s => s.StudentId == studentId && s.KioskId == kioskId)
                    .OrderByDescending(x => x.NotificationTimestamp)
                    .FirstOrDefaultAsync();

            return notification?.NotificationTimestamp;
        }
    }
}
