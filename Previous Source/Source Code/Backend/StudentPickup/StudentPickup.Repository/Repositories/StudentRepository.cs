﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using StudentPickup.Data;

namespace StudentPickup.Repository.Repositories
{
    /// <summary>
    /// The repository to access the Student table in the database
    /// </summary>
    public static class StudentRepository
    {
        /// <summary>
        /// Gets the extra students that will be picked up in the carpool by the student's guardian today. Returns an empty list if no other students are being picked up.
        /// </summary>
        /// <param name="repository">The repository to find the guardian and student in</param>
        /// <param name="guardianId">The unique identifier of the guardian</param>
        /// <param name="schoolId">The unique identifier of the school</param>
        /// <returns>The students that will be picked up by the guardian today. Empty list if no one other than the student is being picked up.</returns>
        public static async Task<List<Student>> GetCarpoolParticipantsByGuardianToday(
            this IRepositoryAsync<Student> repository, int guardianId, int schoolId)
        {
            return await
                repository.Queryable()
                    .Where(
                        student => student.PickupSchedules.Any(
                            pickupSchedule => pickupSchedule.PickupScheduleSessions.Any(
                                pickupScheduleSession =>
                                    pickupScheduleSession.GuardianId == guardianId &&
                                    DbFunctions.TruncateTime(pickupScheduleSession.SessionDate) ==
                                    DbFunctions.TruncateTime(DateTime.UtcNow) &&
                                    pickupScheduleSession.PickupSchedule.Student.SchoolId == schoolId))).ToListAsync();
        }

        /// <summary>
        /// Determines whether the student is being picked up today.
        /// </summary>
        /// <param name="repository">The repository to find the guardian and student in</param>
        /// <param name="studentId">The unique identifier of the student</param>
        /// <returns>Whether the student is being picked up today.</returns>
        public static async Task<bool> StudentBeingPickedUpToday(this IRepositoryAsync<Student> repository,
            int studentId)
        {
            return await repository.Queryable()
                .AnyAsync(
                    student => student.StudentId == studentId && student.PickupSchedules.Any(
                        pickupSchedule => pickupSchedule.PickupScheduleSessions.Any(
                            pickupScheduleSession =>
                                DbFunctions.TruncateTime(pickupScheduleSession.SessionDate) ==
                                DbFunctions.TruncateTime(DateTime.UtcNow))));
        }

        /// <summary>
        /// Gets a student by their rfid.
        /// </summary>
        /// <param name="repository">The repository to find the student in</param>
        /// <param name="rfid">The rfid of the student to find.</param>
        /// <returns>The student with the rfid.</returns>
        public static async Task<Student> GetStudentByRfid(this IRepositoryAsync<Student> repository, string rfid)
        {
            return await repository.Queryable()
                .SingleOrDefaultAsync(student => student.RFID == rfid);
        }
    }
}