﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.Pattern.Repositories;
using StudentPickup.Data;

namespace StudentPickup.Repository.Repositories
{
    /// <summary>
    /// The repository to access the School table in the database
    /// </summary>
    public static class SchoolRepository
    {
        /// <summary>
        /// Gets the school by the KioskId including the GeoZones and GeoZonePolygon Points
        /// </summary>
        /// <param name="repository">The repository to search in</param>
        /// <param name="kioskId">The unique identifier of the kiosk</param>
        /// <returns>The school with the GeoZones and GeoZonePolygon Points</returns>
        public static async Task<School> GetSchoolIncludingGeoZonesByKiosk(this IRepositoryAsync<School> repository, int kioskId)
        {
            return await repository.Queryable()
                .Include(school => school.SchoolGeoZones.Select(geoZone => geoZone.GeoZonePolygonPoints))
                .SingleAsync(school => school.Kiosks.Any(k => k.KioskId == kioskId));
        }
    }
}
