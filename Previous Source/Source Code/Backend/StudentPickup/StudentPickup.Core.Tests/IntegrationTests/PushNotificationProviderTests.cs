﻿using StudentPickup.ApplePushNotificationProvider;
using StudentPickup.Core.PushNotifications;
using StudentPickup.GooglePushNotificationProvider;
using Xunit;

namespace StudentPickup.Core.Tests.IntegrationTests
{
    public class PushNotificationProviderTests
    {
        private readonly IPushNotificationProvider _pushNotificationProvider;

        public PushNotificationProviderTests()
        {
            _pushNotificationProvider = new PushNotificationProvider();
        }

        [Fact]
        public void PushNotificationProviderOperatingSystemIdOneShouldReturnGooglePushNotificationAdapter()
        {
            IPushNotificationAdapter adapter = _pushNotificationProvider.GetPushNotificationAdapter(1);
            Assert.IsType<GooglePushNotificationAdapter>(adapter);
        }

        [Fact]
        public void PushNotificationProviderOperatingSystemIdTwoShouldReturnApplePushNotificationAdapter()
        {
            IPushNotificationAdapter adapter = _pushNotificationProvider.GetPushNotificationAdapter(2);
            Assert.IsType<ApplePushNotificationAdapter>(adapter);
        }

        [Fact]
        public void PushNotificationProviderOperatingSystemIdZeroShouldReturnNull()
        {
            IPushNotificationAdapter adapter = _pushNotificationProvider.GetPushNotificationAdapter(2);
            Assert.Null(adapter);
        }
    }
}
