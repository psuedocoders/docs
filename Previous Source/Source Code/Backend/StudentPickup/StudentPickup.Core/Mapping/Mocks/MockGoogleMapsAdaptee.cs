﻿using System.Threading.Tasks;

namespace StudentPickup.Core.Mapping.Mocks
{
    /// <summary>
    /// Class mocking the functionality of the GoogleMapsApi Wrapper Adaptee
    /// </summary>
    public class MockMapsAdaptee:IMapsTarget
    {
        public Task<int> GetEtaBetweenPoints(decimal? srcLat, decimal? srcLon, decimal? destLat, decimal? destLon)
        {
            return Task.FromResult(1);
        }
    }
}