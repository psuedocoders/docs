﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using GoogleMapsApi.Entities.Directions.Request;

namespace StudentPickup.Core.Mapping
{
    public class GoogleMapsAdaptee
    {
        /// <summary>
        /// Uses Google Maps to calculate the ETA time between two geographical points
        /// </summary>
        /// <param name="srcLat">Source Latitude</param>
        /// <param name="srcLon">Source Longitude</param>
        /// <param name="destLat">Destination Latitude</param>
        /// <param name="destLon">Destination Longitude</param>
        /// <returns>Estimated time of arrival in seconds.</returns>
        public async Task<int> GetEtaBetweenPoints(decimal? srcLat, decimal? srcLon, decimal? destLat, decimal? destLon)
        {
            // if something goes wrong we'll assume the points are lost in space
            if (srcLat == null || srcLon == null || destLat == null || destLon == null)
            {
                throw new ArgumentNullException();
            }
            
            // Get the eta between the guardian and the school using GMaps Api and pass it into to first param of GenerateEtaMessage
            // first we construct the server request
            var directionsRequest = new DirectionsRequest
            {
                Origin = srcLat + "," + srcLon,
                Destination = destLat + "," + destLon,
                ApiKey = ConfigurationManager.AppSettings["GMapsApiKey"]
            };

            // then we contact GMaps with the request and it generates us a response
            //var directionsResponse = GoogleMaps.Directions.QueryAsync(directionsRequest).Result;
            var directionsResponse = await GoogleMapsApi.GoogleMaps.Directions.QueryAsync(directionsRequest);

            // google doesn't give us the full picture straight up so we have to calculate a sum of leg distances
            // a leg being a section of a given route google suggests
            var legs = directionsResponse.Routes.First().Legs;
            var eta = legs.Sum(leg => (int)leg.Duration.Value.Minutes);

            // eta is in minutes, let's return seconds
            return eta * 60;
        }
    }
}