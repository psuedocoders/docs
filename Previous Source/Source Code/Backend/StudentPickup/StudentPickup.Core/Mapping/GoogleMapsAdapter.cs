﻿using System.Threading.Tasks;

namespace StudentPickup.Core.Mapping
{
    public class MapsAdapter : IMapsTarget
    {
        public async Task<int> GetEtaBetweenPoints(decimal? srcLat, decimal? srcLon, decimal? destLat, decimal? destLon)
        {
            var gmaps = new GoogleMapsAdaptee();
            var eta = await gmaps.GetEtaBetweenPoints(srcLat, srcLon, destLat, destLon);

            return eta;
        }
    }
}