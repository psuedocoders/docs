﻿namespace StudentPickup.Core.PushNotifications
{
    /// <summary>
    /// Types of push notifications to be sent to mobile devices
    /// </summary>
    public enum PushNotificationTypes
    {
        // Requests the location of the device
        LocationRequest,
        // Alerts the device of it's current geo zone
        GeozoneInformation,
        // Lets the guardian know that the scheduled guardian can't be contacted
        TrustedGuardianAlert
    }
}
