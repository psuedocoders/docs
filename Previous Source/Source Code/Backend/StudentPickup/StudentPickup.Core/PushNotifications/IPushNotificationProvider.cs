﻿namespace StudentPickup.Core.PushNotifications
{
    /// <summary>
    /// Loads push notitication adapters based on the operating system.
    /// </summary>
    public interface IPushNotificationProvider
    {
        /// <summary>
        /// Gets a push notification adapter for the operating system
        /// </summary>
        /// <param name="operatingSystemId">ID of Operating System</param>
        /// <returns>Push notification adapter for operating system</returns>
        IPushNotificationAdapter GetPushNotificationAdapter(int operatingSystemId);
    }
}
