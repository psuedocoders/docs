﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.Practices.Unity;

namespace StudentPickup.Core.PushNotifications
{
    /// <summary>
    /// Loads push notitication adapters based on the operating system using MEF.
    /// </summary>
    public class PushNotificationProvider : IPushNotificationProvider
    {
        //Load set of pushProvider instances with corresponding metadata
        [ImportMany(typeof(IPushNotificationAdapter))]
        IEnumerable<Lazy<IPushNotificationAdapter, IPushNotificationAdapterMetadata>> _providerData;
        
        /// <summary>
        /// Gets a push notification adapter for the operating system
        /// </summary>
        /// <param name="operatingSystemId">ID of Operating System</param>
        /// <returns>Push notification adapter for operating system</returns>
        public IPushNotificationAdapter GetPushNotificationAdapter(int operatingSystemId)
        {
            var adapter = _providerData.FirstOrDefault(i => i.Metadata.OperatingSystemId == operatingSystemId);
            return adapter?.Value;
        }
    }
}
