﻿using System.Collections.Generic;

namespace StudentPickup.Core.PushNotifications.Mocks
{
    /// <summary>
    /// A mock push notification adapater
    /// </summary>
    public class MockPushNotificationAdapter : IPushNotificationAdapter
    {
        /// <summary>
        /// Send a mock push notification
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="pushNotificationType"></param>
        /// <param name="alert"></param>
        /// <returns></returns>
        public bool SendPushNotification(string deviceId, PushNotificationTypes pushNotificationType, string alert)
        {
            return true;
        }

        /// <summary>
        /// Send a mock push notification
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="pushNotificationType"></param>
        /// <param name="alert"></param>
        /// <param name="customData"></param>
        /// <returns></returns>
        public bool SendPushNotification(string deviceId, PushNotificationTypes pushNotificationType, string alert,
            Dictionary<string, string> customData)
        {
            return true;
        }
    }
}
