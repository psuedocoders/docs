﻿namespace StudentPickup.Core.PushNotifications.Mocks
{
    /// <summary>
    /// Mock push notification adapter that returns a mock push notification provider
    /// </summary>
    public class MockPushNotificationProvider : IPushNotificationProvider
    {
        /// <summary>
        /// Returns a mock push notification adapter
        /// </summary>
        /// <param name="operatingSystemId">Operating system ID of device</param>
        /// <returns>Mock push notification adapter</returns>
        public IPushNotificationAdapter GetPushNotificationAdapter(int operatingSystemId)
        {
            return new MockPushNotificationAdapter();
        }
    }
}
