﻿using System;
using PushSharp.Core;

namespace StudentPickup.Core.PushNotifications
{
    /// <summary>
    /// An adapter for a push notification service to facilitate sending push notifications to mobile devices
    /// </summary>
    public abstract class PushNotificationAdapter
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Gets the name of the type of the push notification being sent
        /// </summary>
        /// <param name="pushNotificationType">The type of the push notification to send</param>
        /// <returns>The name of the push notification being sent</returns>
        protected string GetPushNotificationTypeString(PushNotificationTypes pushNotificationType)
        {
            switch (pushNotificationType)
            {
                case PushNotificationTypes.LocationRequest:
                    return "locationRequest";
                case PushNotificationTypes.GeozoneInformation:
                    return "geozoneInformation";
                case PushNotificationTypes.TrustedGuardianAlert:
                    return "trustedGuardianAlert";
                default:
                    throw new ArgumentOutOfRangeException(nameof(pushNotificationType), pushNotificationType, null);
            }
        }

        protected virtual void NotificationFailed(object sender, INotification notification, Exception error)
        {
            Log.Error("Notification Failed", error);
        }

        protected virtual void ServiceException(object sender, Exception error)
        {
            Log.Error("Service Exception", error);
        }

        protected virtual void ChannelException(object sender, IPushChannel pushChannel, Exception error)
        {
            Log.Error("Channel Exception", error);
        }
        
        protected virtual void LogError(Exception exception)
        {
            Log.Error(exception);
        }
    }
}
