﻿namespace StudentPickup.Core.PushNotifications
{
    /// <summary>
    /// An interface for the push notification Managed Extensibility Framework metdata to facilitate dependency injection for the push notification services.
    /// </summary>
    public interface IPushNotificationAdapterMetadata
    {
        // The name of the operating system of the push notification service
        string OperatingSystemName { get; }

        // The id of the operating system of the push notification service
        int OperatingSystemId { get; }
    }

}
