﻿namespace StudentPickup.Core.Models
{
    public class KioskRequest
    {
        public int KioskId { get; set; }
        public int PortId { get; set; }
        public string Rfid { get; set; }
        public int StudentId { get; set; }
        public int SchoolId { get; set; }
        public int? Response { get; set; } //needs to be nullable as there isn't always a reponse, e.g. on first tap
    }
}
