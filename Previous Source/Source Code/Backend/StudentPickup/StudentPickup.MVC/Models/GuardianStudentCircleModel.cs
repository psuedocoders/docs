﻿using StudentPickup.Data;

namespace StudentPickup.MVC.Models
{
    public class GuardianStudentCircleModel
    {
        public int StudentId { get; set; }
        public int GuardianId { get; set; }
        public CircleTypes CircleTypeId { get; set; }
    }
}