﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Service;

namespace StudentPickup.MVC.Controllers
{
    public class ManageSchoolsController : Controller
    {
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        private readonly ISchoolService _schoolService;
        private readonly IStateService _stateService;

        public ManageSchoolsController(IUnitOfWorkAsync unitOfWorkAsync, ISchoolService schoolService, IStateService stateService)
        {
            _unitOfWorkAsync = unitOfWorkAsync;
            _schoolService = schoolService;
            _stateService = stateService;
        }

        // GET: ManageSchools
        public async Task<ActionResult> Index()
        {
            var schools = _schoolService.Queryable().Include(s => s.State);
            return View(await schools.ToListAsync());
        }

        // GET: ManageSchools/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var school = await _schoolService.Queryable().Include(s => s.State).SingleOrDefaultAsync(s => s.SchoolId == id);
            if (school == null)
            {
                return HttpNotFound();
            }
            return View(school);
        }

        // GET: ManageSchools/Create
        public ActionResult Create()
        {
            ViewBag.StateId = new SelectList(_stateService.Queryable(), "StateId", "Name");
            return View();
        }

        // POST: ManageSchools/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "SchoolId,Name,Address,ContactName,ContactEmail,ContactNumber,StateId,Latitude,Longtitude")] School school)
        {
            if (ModelState.IsValid)
            {
                _schoolService.Insert(school);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.StateId = new SelectList(_stateService.Queryable(), "StateId", "Name", school.StateId);
            return View(school);
        }

        // GET: ManageSchools/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var school = await _schoolService.Queryable().Include(s => s.State).SingleOrDefaultAsync(s => s.SchoolId == id);
            if (school == null)
            {
                return HttpNotFound();
            }
            ViewBag.StateId = new SelectList(_stateService.Queryable(), "StateId", "Name", school.StateId);
            return View(school);
        }

        // POST: ManageSchools/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "SchoolId,Name,Address,ContactName,ContactEmail,ContactNumber,StateId,Latitude,Longtitude")] School school)
        {
            if (ModelState.IsValid)
            {
                _schoolService.Update(school);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.StateId = new SelectList(_stateService.Queryable(), "StateId", "Name", school.StateId);
            return View(school);
        }

        // GET: ManageSchools/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var school = await _schoolService.Queryable().Include(s => s.State).SingleOrDefaultAsync(s => s.SchoolId == id);
            if (school == null)
            {
                return HttpNotFound();
            }
            return View(school);
        }

        // POST: ManageSchools/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var school = await _schoolService.FindAsync(id);
            _schoolService.Delete(school);
            await _unitOfWorkAsync.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWorkAsync.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
