﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Service;

namespace StudentPickup.MVC.Controllers
{
    public class ManageKiosksController : Controller
    {
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        private readonly IKioskService _kioskService;
        private readonly ISchoolService _schoolService;

        public ManageKiosksController(IUnitOfWorkAsync unitOfWorkAsync, IKioskService kioskService, ISchoolService schoolService)
        {
            _unitOfWorkAsync = unitOfWorkAsync;
            _kioskService = kioskService;
            _schoolService = schoolService;
        }
        // GET: Kiosks
        public async Task<ActionResult> Index()
        {
            var kiosks = _kioskService.Queryable().Include(k => k.ParentKiosk).Include(k => k.School);
            return View(await kiosks.ToListAsync());
        }

        // GET: Kiosks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var kiosk = await _kioskService.FindAsync(id);
            if (kiosk == null)
            {
                return HttpNotFound();
            }
            return View(kiosk);
        }

        // GET: Kiosks/Create
        public ActionResult Create()
        {
            ViewBag.ParentKioskId = new SelectList(_kioskService.Queryable(), "KioskId", "Name");
            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name");
            return View();
        }

        // POST: Kiosks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "KioskId,ParentKioskId,ParentPortNumber,SchoolId,Name,Latitude,Longtitude")] Kiosk kiosk)
        {
            if (ModelState.IsValid)
            {
                _kioskService.Insert(kiosk);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ParentKioskId = new SelectList(_kioskService.Queryable(), "KioskId", "Name", kiosk.ParentKioskId);
            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name", kiosk.SchoolId);
            return View(kiosk);
        }

        // GET: Kiosks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var kiosk = await _kioskService.FindAsync(id);
            if (kiosk == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentKioskId = new SelectList(_kioskService.Queryable(), "KioskId", "Name", kiosk.ParentKioskId);
            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name", kiosk.SchoolId);
            return View(kiosk);
        }

        // POST: Kiosks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "KioskId,ParentKioskId,ParentPortNumber,SchoolId,Name,Latitude,Longtitude")] Kiosk kiosk)
        {
            if (ModelState.IsValid)
            {
                _kioskService.Update(kiosk);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ParentKioskId = new SelectList(_kioskService.Queryable(), "KioskId", "Name", kiosk.ParentKioskId);
            ViewBag.SchoolId = new SelectList(_schoolService.Queryable(), "SchoolId", "Name", kiosk.SchoolId);
            return View(kiosk);
        }

        // GET: Kiosks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var kiosk = await _kioskService.FindAsync(id);
            if (kiosk == null)
            {
                return HttpNotFound();
            }
            return View(kiosk);
        }

        // POST: Kiosks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var kiosk = await _kioskService.FindAsync(id);
            _kioskService.Delete(kiosk);
            await _unitOfWorkAsync.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWorkAsync.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
