﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Service;

namespace StudentPickup.MVC.Controllers
{
    public class ManagePickupScheduleSessionsController : Controller
    {
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        private readonly IPickupScheduleSessionService _pickupScheduleSessionService;
        private readonly IPickupScheduleService _pickupScheduleService;
        private readonly IGuardianService _guardianService;

        public ManagePickupScheduleSessionsController(IUnitOfWorkAsync unitOfWorkAsync,
            IPickupScheduleSessionService pickupScheduleSessionService, IPickupScheduleService pickupScheduleService,
            IGuardianService guardianService)
        {
            _unitOfWorkAsync = unitOfWorkAsync;
            _pickupScheduleService = pickupScheduleService;
            _guardianService = guardianService;
            _pickupScheduleSessionService = pickupScheduleSessionService;
        }

        // GET: ManagePickupScheduleSessions
        public async Task<ActionResult> Index()
        {
            var pickupScheduleSessions = _pickupScheduleSessionService.Queryable().Include(p => p.Guardian).Include(p => p.PickupSchedule.Student);
            return View(await pickupScheduleSessions.ToListAsync());
        }

        // GET: ManagePickupScheduleSessions/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pickupScheduleSession = await _pickupScheduleSessionService.FindAsync(id);
            if (pickupScheduleSession == null)
            {
                return HttpNotFound();
            }
            return View(pickupScheduleSession);
        }

        // GET: ManagePickupScheduleSessions/Create
        public ActionResult Create()
        {
            ViewBag.PickupScheduleId = new SelectList(_pickupScheduleService.Queryable()
                .Include(p => p.Student)
                .AsEnumerable()
                .Select(p =>
                new {
                    p.PickupScheduleId,
                    StudentName = $"{p.StartDate.Value.ToShortDateString()} - {p.EndDate.Value.ToShortDateString()} - {p.Student.GivenName} {p.Student.FamilyName}"
                }), "PickupScheduleId", "StudentName");
            return View();
        }

        // POST: ManagePickupScheduleSessions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "PickupScheduleSessionId,PickupScheduleId,GuardianId,SessionDate")] PickupScheduleSession pickupScheduleSession)
        {
            if (ModelState.IsValid)
            {
                _pickupScheduleSessionService.Insert(pickupScheduleSession);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            
            ViewBag.PickupScheduleId = new SelectList(_pickupScheduleService.Queryable()
                .Include(p => p.Student)
                .AsEnumerable()
                .Select(p =>
                new {
                    p.PickupScheduleId,
                    StudentName = $"{p.StartDate.Value.ToShortDateString()} - {p.EndDate.Value.ToShortDateString()} - {p.Student.GivenName} {p.Student.FamilyName}"
                }), "PickupScheduleId", "StudentName");
            return View(pickupScheduleSession);
        }

        // GET: ManagePickupScheduleSessions/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pickupScheduleSession = await _pickupScheduleSessionService.FindAsync(id);
            if (pickupScheduleSession == null)
            {
                return HttpNotFound();
            }
            ViewBag.PickupScheduleId = new SelectList(_pickupScheduleService.Queryable()
                .Include(p => p.Student)
                .AsEnumerable()
                .Select(p =>
                new {
                    p.PickupScheduleId,
                    StudentName = $"{p.StartDate.Value.ToShortDateString()} - {p.EndDate.Value.ToShortDateString()} - {p.Student.GivenName} {p.Student.FamilyName}"
                }), "PickupScheduleId", "StudentName");
            return View(pickupScheduleSession);
        }

        // POST: ManagePickupScheduleSessions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "PickupScheduleSessionId,PickupScheduleId,GuardianId,SessionDate")] PickupScheduleSession pickupScheduleSession)
        {
            if (ModelState.IsValid)
            {
                _pickupScheduleSessionService.Update(pickupScheduleSession);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PickupScheduleId = new SelectList(_pickupScheduleService.Queryable()
                .Include(p => p.Student)
                .AsEnumerable()
                .Select(p =>
                new {
                    p.PickupScheduleId,
                    StudentName = $"{p.StartDate.Value.ToShortDateString()} - {p.EndDate.Value.ToShortDateString()} - {p.Student.GivenName} {p.Student.FamilyName}"
                }), "PickupScheduleId", "StudentName");
            return View(pickupScheduleSession);
        }

        // GET: ManagePickupScheduleSessions/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pickupScheduleSession = await _pickupScheduleSessionService.FindAsync(id);
            if (pickupScheduleSession == null)
            {
                return HttpNotFound();
            }
            return View(pickupScheduleSession);
        }

        // POST: ManagePickupScheduleSessions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var pickupScheduleSession = await _pickupScheduleSessionService.FindAsync(id);
            _pickupScheduleSessionService.Delete(pickupScheduleSession);
            await _unitOfWorkAsync.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWorkAsync.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
