﻿using System;
using System.Data.Entity.Core.EntityClient;
using System.IO;
using Effort.DataLoaders;
using Repository.Pattern.DataContext;
using Repository.Pattern.Ef6;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using StudentPickup.Data;
using StudentPickup.Repository.Repositories;
using Xunit;

namespace StudentPickup.Repository.Tests.IntegrationTests
{
    public class GuardianStudentCircleRepositoryTests
    {
        //Create in memeory test entity connection to make unit testing easier
        private readonly EntityConnection _connection;
        private readonly IDataContextAsync _studentPickupContext;
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IRepositoryAsync<GuardianStudentCircle> _guardianStudentCircleRepository;

        public GuardianStudentCircleRepositoryTests()
        {
            // Loads csv files from TestData folder, csv file per table
            IDataLoader loader = new CsvDataLoader(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData"));

            // Setup In Memory Database Context
            _connection = Effort.EntityConnectionFactory.CreateTransient("name=StudentPickupEntities", loader);
            _studentPickupContext = new StudentPickupEntities(_connection);
            _unitOfWork = new UnitOfWork(_studentPickupContext);

            // Setup Repositories
            _guardianStudentCircleRepository = new Repository<GuardianStudentCircle>(_studentPickupContext, _unitOfWork);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
            _studentPickupContext.Dispose();
            _connection.Dispose();
        }
        
        [Fact]
        public async void GuardianExistsInStudentCircleGuardianExistsShouldReturnTrue()
        {
            var guardianExists = await _guardianStudentCircleRepository.GuardianExistsInStudentCircle(1, 1);
            Assert.True(guardianExists);
        }

        [Fact]
        public async void GuardianExistsInStudentCircleGuardianDoesntExistShouldReturnFalse()
        {
            var guardianExists = await _guardianStudentCircleRepository.GuardianExistsInStudentCircle(1, 12);
            Assert.False(guardianExists);
        }

        [Fact]
        public async void RemoveGuardianFromStudentCircleGuardianIsInCircleShouldSucceed()
        {
            await _guardianStudentCircleRepository.RemoveGuardianFromStudentCircle(1, 1);
            await _unitOfWork.SaveChangesAsync();
            var guardianExists = await _guardianStudentCircleRepository.GuardianExistsInStudentCircle(1, 1);
            Assert.False(guardianExists);
        }

        [Fact]
        public async void RemoveGuardianFromStudentCircleGuardianIsNotInCircleShouldThrowException()
        {
            await Assert.ThrowsAsync<Exception>(async () =>
                await _guardianStudentCircleRepository.RemoveGuardianFromStudentCircle(1, 12));
        }
    }
}
