﻿using System;
using System.IO;
using Effort;
using Effort.DataLoaders;
using Repository.Pattern.Ef6;
using Repository.Pattern.Repositories;
using StudentPickup.Data;
using Xunit;

namespace StudentPickup.Service.Tests.IntegrationTests
{
    public class StudentServiceTests
    {
        private readonly IStudentService _studentService;
        private readonly IRepositoryAsync<PickupScheduleSession> _pickupScheduleSessionRepository;
        private readonly UnitOfWork _unitOfWork;

        public StudentServiceTests()
        {
            // Loads csv files from TestData folder, csv file per table
            IDataLoader loader = new CsvDataLoader(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData"));

            // Setup In Memory Database Context
            var connection = EntityConnectionFactory.CreateTransient("name=StudentPickupEntities", loader);
            var studentPickupContext = new StudentPickupEntities(connection);
            _unitOfWork = new UnitOfWork(studentPickupContext);

            // Setup Repositories
            var studentRepository = new Repository<Student>(studentPickupContext, _unitOfWork);
            _pickupScheduleSessionRepository = new Repository<PickupScheduleSession>(studentPickupContext, _unitOfWork);

            // Setup Services
            _studentService = new StudentService(studentRepository);
        }


        [Fact]
        public async void CheckForCarpoolStudentIsInCarpool()
        {
            var session = await _pickupScheduleSessionRepository.FindAsync(4);
            session.SessionDate = DateTime.Now;
            _pickupScheduleSessionRepository.Update(session);
            await _unitOfWork.SaveChangesAsync();

            session = await _pickupScheduleSessionRepository.FindAsync(41);
            session.SessionDate = DateTime.Now;
            _pickupScheduleSessionRepository.Update(session);
            await _unitOfWork.SaveChangesAsync();

            var carpoolExists = await _studentService.IsStudentInCarpoolToday(4, 7);
            Assert.True(carpoolExists);
        }

        [Fact]
        public async void CheckForCarpoolStudentIsNotInCarpool()
        {
            var session = await _pickupScheduleSessionRepository.FindAsync(42);
            session.SessionDate = DateTime.Now;
            _pickupScheduleSessionRepository.Update(session);
            await _unitOfWork.SaveChangesAsync();

            var carpoolExists = await _studentService.IsStudentInCarpoolToday(4, 7);
            Assert.False(carpoolExists);
        }

        [Fact]
        public async void StudentBeingPickedUpTodayBeingPickedUpToday()
        {
            var session = await _pickupScheduleSessionRepository.FindAsync(4);
            session.SessionDate = DateTime.Now;
            _pickupScheduleSessionRepository.Update(session);
            await _unitOfWork.SaveChangesAsync();

            var studentBeingPickedUpToday = await _studentService.StudentBeingPickedUpToday(4);
            Assert.True(studentBeingPickedUpToday);
        }

        [Fact]
        public async void StudentBeingPickedUpTodayNotBeingPickedUpToday()
        {
            var session = await _pickupScheduleSessionRepository.FindAsync(43);
            session.SessionDate = DateTime.Now.AddDays(2);
            _pickupScheduleSessionRepository.Update(session);
            await _unitOfWork.SaveChangesAsync();

            var studentBeingPickedUpToday = await _studentService.StudentBeingPickedUpToday(43);
            Assert.False(studentBeingPickedUpToday);
        }
        
        [Fact]
        public async void GetStudentByRfidStudentExistsShouldReturnStudent()
        {
            var student = await _studentService.GetStudentByRfid("SOMERFID1");
            
            Assert.Equal(1, student.StudentId);
        }
    }
}

