﻿using System;
using System.IO;
using System.Linq;
using Effort;
using Effort.DataLoaders;
using Repository.Pattern.Ef6;
using StudentPickup.Core.Mapping;
using StudentPickup.Core.Mapping.Mocks;
using StudentPickup.Core.PushNotifications;
using StudentPickup.Core.PushNotifications.Mocks;
using StudentPickup.Data;
using Xunit;

namespace StudentPickup.Service.Tests.IntegrationTests
{
    public class GuardianStudentCircleServiceTests
    {
        private readonly IGuardianStudentCircleService _guardianStudentCircleService;
        private readonly IGuardianService _guardianService;
        private readonly UnitOfWork _unitOfWork;

        public GuardianStudentCircleServiceTests()
        {
            // Loads csv files from TestData folder, csv file per table
            IDataLoader loader = new CsvDataLoader(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData"));

            // Setup In Memory Database Context
            var connection = EntityConnectionFactory.CreateTransient("name=StudentPickupEntities", loader);
            var studentPickupContext = new StudentPickupEntities(connection);
            _unitOfWork = new UnitOfWork(studentPickupContext);

            // Setup providers
            IMapsTarget mapsTarget = new MockMapsAdapter();
            IPushNotificationProvider pushNotificationProvider = new MockPushNotificationProvider();

            // Setup Repositories
            var guardianRepository = new Repository<Guardian>(studentPickupContext, _unitOfWork);
            var guardianDeviceRepository = new Repository<GuardianDevice>(studentPickupContext, _unitOfWork);
            var guardianStudentCircleRepository = new Repository<GuardianStudentCircle>(studentPickupContext, _unitOfWork);
            var studentCircleRepository = new Repository<StudentCircle>(studentPickupContext, _unitOfWork);

            // Setup Services
            var guardianDeviceService = new GuardianDeviceService(guardianDeviceRepository, pushNotificationProvider);
            _guardianStudentCircleService = new GuardianStudentCircleService(guardianStudentCircleRepository, studentCircleRepository, _unitOfWork);
            _guardianService = new GuardianService(guardianRepository, mapsTarget, guardianDeviceService);
        }

        [Fact]
        public async void AddGuardianToStudentCircleGuardianIsntInTrustedCircleShouldAddGuardian()
        {
            await _guardianStudentCircleService.AddGuardianToStudentCircle(12, 13, CircleTypes.Trusted);
            await _unitOfWork.SaveChangesAsync();
            var guardians = await _guardianService.GetGuardiansInCircleByStudentId(13, CircleTypes.Trusted);
            var guardianExists = guardians.Any(g => g.GuardianId == 12);
            Assert.True(guardianExists);
        }

        [Fact]
        public async void AddGuardianToStudentCircleGuardianIsInTrustedCircleShouldThrowException()
        {
            await
                Assert.ThrowsAsync<Exception>(
                    async () =>
                        await _guardianStudentCircleService.AddGuardianToStudentCircle(1, 13, CircleTypes.Trusted));
        }

        [Fact]
        public async void AddGuardianToStudentCircleGuardianIsntInScheduledCircleShouldAddGuardian()
        {
            await _guardianStudentCircleService.AddGuardianToStudentCircle(12, 13, CircleTypes.Scheduled);
            await _unitOfWork.SaveChangesAsync();
            var guardians = await _guardianService.GetGuardiansInCircleByStudentId(13, CircleTypes.Scheduled);
            var guardianExists = guardians.Any(g => g.GuardianId == 12);
            Assert.True(guardianExists);
        }

        [Fact]
        public async void AddGuardianToStudentCircleGuardianIsInScheduledCircleShouldThrowException()
        {
            await
                Assert.ThrowsAsync<Exception>(
                    async () =>
                        await _guardianStudentCircleService.AddGuardianToStudentCircle(1, 13, CircleTypes.Scheduled));
        }

        [Fact]
        public async void RemoveGuardianFromStudentCircleGuardianExistsInTrustedCircleShowRemoveGuardian()
        {
            await _guardianStudentCircleService.RemoveGuardianFromStudentCircle(1, 13, CircleTypes.Trusted);
            await _unitOfWork.SaveChangesAsync();
            var guardians = await _guardianService.GetGuardiansInCircleByStudentId(13, CircleTypes.Trusted);
            var guardianExists = guardians.Any(g => g.GuardianId == 1);
            Assert.False(guardianExists);
        }

        [Fact]
        public async void RemoveGuardianFromStudentCircleGuardianExistsInScheduledCircleShowRemoveGuardian()
        {
            await _guardianStudentCircleService.RemoveGuardianFromStudentCircle(1, 13, CircleTypes.Scheduled);
            await _unitOfWork.SaveChangesAsync();
            var guardians = await _guardianService.GetGuardiansInCircleByStudentId(13, CircleTypes.Scheduled);
            var guardianExists = guardians.Any(g => g.GuardianId == 1);
            Assert.False(guardianExists);
        }
    }
}