﻿using System;
using System.Collections.Generic;
using StudentPickup.Data;
using Xunit;

namespace StudentPickup.Service.Tests.UnitTests
{
    public class GuardianServiceTests
    {
        [Fact]
        public void EtaMessageNegativeEtaNegativeTransactionTimeShouldThrowException()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Red};
            Assert.Throws<ArgumentException>(() => GuardianService.GenerateEtaMessage(-1, guardianGeoZone));
        }

        [Fact]
        public void EtaMessageGuardianIsCloseInLegalZone()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Green};
            var response = GuardianService.GenerateEtaMessage(1, guardianGeoZone);
            Assert.Equal("Your parent or guardian is less than 3 minutes away. Please proceed to the pickup zone.",
                response.Item1);
            Assert.Equal(0, response.Item2);
        }

        [Fact]
        public void EtaMessageGuardianIsCloseUpperBoundInLegalZone()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Green};
            var response = GuardianService.GenerateEtaMessage(120, guardianGeoZone);
            Assert.Equal("Your parent or guardian is less than 3 minutes away. Please proceed to the pickup zone.",
                response.Item1);
            Assert.Equal(0, response.Item2);
        }

        [Fact]
        public void EtaMessageGuardianIsCloseTestRoundingInLegalZone()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Green};
            var response = GuardianService.GenerateEtaMessage(121, guardianGeoZone);
            Assert.Equal("Your parent or guardian is less than 3 minutes away. Please proceed to the pickup zone.",
                response.Item1);
            Assert.Equal(0, response.Item2);
        }

        [Fact]
        public void EtaMessageGuardianIsCloseInIllegalZone()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Red};
            var response = GuardianService.GenerateEtaMessage(120, guardianGeoZone);
            Assert.Equal("Your parent or guardian is is close but not ready yet. \nPlease try again in a few minutes.",
                response.Item1);
            Assert.Equal(1, response.Item2);
        }

        [Fact]
        public void EtaMessageGuardianIsApproachingInLegalZone()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Green};
            var response = GuardianService.GenerateEtaMessage(300, guardianGeoZone);
            Assert.Equal("Your parent or guardian is on their way. \n" +
                         "Their estimated time of arrival will be in 5 minutes. \n" +
                         "Please try again in a few minutes.", response.Item1);
            Assert.Equal(1, response.Item2);
        }

        [Fact]
        public void EtaMessageGuardianIsApproachingInIllegalZone()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Red};
            var response = GuardianService.GenerateEtaMessage(300, guardianGeoZone);
            Assert.Equal("Your parent or guardian is on their way. \n" +
                         "Their estimated time of arrival will be in 5 minutes. \n" +
                         "Please try again in a few minutes.", response.Item1);
            Assert.Equal(1, response.Item2);
        }

        [Fact]
        public void EtaMessageGuardianIsApproachingUpperBoundInLegalZoneLongTransactionTime()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Green};
            var response = GuardianService.GenerateEtaMessage(900, guardianGeoZone);
            Assert.Equal("Your parent or guardian is on their way. \n" +
                         "Their estimated time of arrival will be in 15 minutes. \n" +
                         "Please try again in a few minutes.", response.Item1);
            Assert.Equal(1, response.Item2);
        }

        [Fact]
        public void EtaMessageGuardianIsApproachingTestRoundingInLegalZoneLongTransactionTime()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Green};
            var response = GuardianService.GenerateEtaMessage(901, guardianGeoZone);
            Assert.Equal("Your parent or guardian is on their way. \n" +
                         "Their estimated time of arrival will be in 15 minutes. \n" +
                         "Please try again in a few minutes.", response.Item1);
            Assert.Equal(1, response.Item2);
        }

        [Fact]
        public void EtaMessageGuardianIsFarInLegalZoneLongTransactionTime()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Green};
            var response = GuardianService.GenerateEtaMessage(1000, guardianGeoZone);
            Assert.Equal("Please go to school after care. \n" +
                         "Your guardian has been told to pick you up there.", response.Item1);
            Assert.Equal(1, response.Item2);
        }

        [Fact]
        public void EtaMessageGuardianIsFarInLegalZoneShortTransactionTimeUpperBound()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Green};
            var response = GuardianService.GenerateEtaMessage(960, guardianGeoZone);
            Assert.Equal("Please go to school after care. \n" +
                         "Your guardian has been told to pick you up there.", response.Item1);
            Assert.Equal(1, response.Item2);
        }

        [Fact]
        public void EtaMessageGuardianIsFarInLegalZoneShortTransaction()
        {
            var guardianGeoZone = new SchoolGeoZone {GeoZoneTypeId = GeoZoneTypes.Green};
            var response = GuardianService.GenerateEtaMessage(960, guardianGeoZone);
            Assert.Equal("Please go to school after care. \n" +
                         "Your guardian has been told to pick you up there.", response.Item1);
            Assert.Equal(1, response.Item2);
        }


        /// <summary>
        /// Geozone is a simple rectangle, guardian in the middle. 
        /// 
        /// Expected: True.
        /// </summary>
        [Fact]
        public void TestGuardianServiceAlgorithmBasicRectangleShouldReturnTrue()
        {
            // Create a geozone (for reference, this geozone is the block in front of Caulfield Grammar School).
            var nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877363, Longtitude = (decimal) 145.001928};
            var swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877441, Longtitude = (decimal) 145.001939};
            var neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877789, Longtitude = (decimal) 145.005345};
            var seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877852, Longtitude = (decimal) 145.005334};
            var list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var caulfieldGrammarGeozone = new SchoolGeoZone {GeoZonePolygonPoints = list};

            Assert.Equal(
                GuardianService.GuardianInGeoZone(caulfieldGrammarGeozone, (decimal) -37.877534, (decimal) 145.003022),
                true);
        }

        /// <summary>
        /// Make the shape of the polygon a little "weird", by including two more points that cut the polygon
        /// near the middle, creating a pizza shaped hole. 
        /// 
        /// Guardian is in the middle, to the right of the hole. 
        /// 
        /// Expected: True. 
        /// </summary>
        [Fact]
        public void TestGuardianServiceAlgorithmComplexPolygonShouldReturnTrue()
        {
            // Create a geozone (for reference, this geozone is the block in front of Caulfield Grammar School).
            var nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877363, Longtitude = (decimal) 145.001928};
            var swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877441, Longtitude = (decimal) 145.001939};
            var neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877789, Longtitude = (decimal) 145.005345};
            var seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877852, Longtitude = (decimal) 145.005334};
            var southCutOffPoint = new GeoZonePolygonPoint
            {
                Latitude = (decimal) -37.877407,
                Longtitude = (decimal) 145.002100
            };
            var southReturnPoint = new GeoZonePolygonPoint
            {
                Latitude = (decimal) -37.877501,
                Longtitude = (decimal) 145.002314
            };
            var list = new List<GeoZonePolygonPoint>
            {
                swCorner,
                nwCorner,
                neCorner,
                seCorner,
                southCutOffPoint,
                southReturnPoint
            };
            var caulfieldGrammarGeozone = new SchoolGeoZone {GeoZonePolygonPoints = list};

            Assert.Equal(
                GuardianService.GuardianInGeoZone(caulfieldGrammarGeozone, (decimal) -37.877730, (decimal) 145.004601),
                true);
        }

        /// <summary>
        /// Geozone is an almost rectangle, guardian is exactly on the western border. 
        /// 
        /// Expected: true. 
        /// </summary>
        [Fact]
        public void TestGuardianServiceAlgorithmRectangleBorderCaseShouldReturnTrue()
        {
            // Create a geozone (for reference, this geozone is the block in front of Caulfield Grammar School).
            var nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877363, Longtitude = (decimal) 145.001866};
            var swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877441, Longtitude = (decimal) 145.001866};
            var neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877789, Longtitude = (decimal) 145.005345};
            var seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877852, Longtitude = (decimal) 145.005334};
            var list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var caulfieldGrammarGeozone = new SchoolGeoZone {GeoZonePolygonPoints = list};

            Assert.Equal(
                GuardianService.GuardianInGeoZone(caulfieldGrammarGeozone, (decimal) -37.877390, (decimal) 145.001928),
                true);
        }

        /// <summary>
        /// Geozone is an almost rectangle, guardian is exactly on the western border. Also, there is a hole in the middle similar
        /// to Success2. 
        /// 
        /// Expected: true. 
        /// </summary>
        [Fact]
        public void TestGuardianServiceAlgorithmComplexPolygonBorderCaseShouldReturnTrue()
        {
            // Create a geozone (for reference, this geozone is the block in front of Caulfield Grammar School).
            var nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877363, Longtitude = (decimal) 145.001866};
            var swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877441, Longtitude = (decimal) 145.001866};
            var neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877789, Longtitude = (decimal) 145.005345};
            var seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877852, Longtitude = (decimal) 145.005334};
            var southCutOffPoint = new GeoZonePolygonPoint
            {
                Latitude = (decimal) -37.877407,
                Longtitude = (decimal) 145.002100
            };
            var southReturnPoint = new GeoZonePolygonPoint
            {
                Latitude = (decimal) -37.877501,
                Longtitude = (decimal) 145.002314
            };
            var list = new List<GeoZonePolygonPoint>
            {
                swCorner,
                nwCorner,
                neCorner,
                seCorner,
                southCutOffPoint,
                southReturnPoint
            };
            var caulfieldGrammarGeozone = new SchoolGeoZone {GeoZonePolygonPoints = list};

            Assert.Equal(
                GuardianService.GuardianInGeoZone(caulfieldGrammarGeozone, (decimal) -37.877390, (decimal) 145.001928),
                true);
        }

        /// <summary>
        /// Simple rectangle geozone, guardian outside of the polygon. 
        /// 
        /// Expected: false.
        /// </summary>
        [Fact]
        public void TestGuardianServiceAlgorithmOutsideRectangleShouldReturnFalse()
        {
            // Create a geozone (for reference, this geozone is the block in front of Caulfield Grammar School).
            var nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877363, Longtitude = (decimal) 145.001928};
            var swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877441, Longtitude = (decimal) 145.001939};
            var neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877789, Longtitude = (decimal) 145.005345};
            var seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877852, Longtitude = (decimal) 145.005334};
            var list = new List<GeoZonePolygonPoint> {swCorner, neCorner, nwCorner, seCorner};
            var caulfieldGrammarGeozone = new SchoolGeoZone {GeoZonePolygonPoints = list};

            Assert.Equal(
                GuardianService.GuardianInGeoZone(caulfieldGrammarGeozone, (decimal) -37.877365, (decimal) 145.001590),
                false);
        }

        /// <summary>
        /// Polygon is cut (as in success2), guardian is placed in the resulting hole. 
        /// 
        /// Expected: false.
        /// </summary>
        [Fact]
        public void TestGuardianServiceAlgorithmOutsideComplexPolygonShouldReturnFalse()
        {
            // Create a geozone (for reference, this geozone is the block in front of Caulfield Grammar School).
            var nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877363, Longtitude = (decimal) 145.001928};
            var swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877441, Longtitude = (decimal) 145.001939};
            var neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877789, Longtitude = (decimal) 145.005345};
            var seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877852, Longtitude = (decimal) 145.005334};
            var southCutOffPoint = new GeoZonePolygonPoint
            {
                Latitude = (decimal) -37.877407,
                Longtitude = (decimal) 145.002100
            };
            var southReturnPoint = new GeoZonePolygonPoint
            {
                Latitude = (decimal) -37.877501,
                Longtitude = (decimal) 145.002314
            };
            var list = new List<GeoZonePolygonPoint>
            {
                swCorner,
                nwCorner,
                neCorner,
                seCorner,
                southCutOffPoint,
                southReturnPoint
            };
            var caulfieldGrammarGeozone = new SchoolGeoZone {GeoZonePolygonPoints = list};

            Assert.Equal(
                GuardianService.GuardianInGeoZone(caulfieldGrammarGeozone, (decimal) -37.877501, (decimal) 145.002314),
                false);
        }

        /// <summary>
        /// The guardian is placed in a redzone. 
        /// 
        /// Expected: The GuardianService method returns the correct geozone. 
        /// </summary>
        [Fact]
        public void TestGuardianServiceRedzoneShouldReturnRedZone()
        {
            // Create a red geozone
            var nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877363, Longtitude = (decimal) 145.001928};
            var swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877441, Longtitude = (decimal) 145.001939};
            var neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877789, Longtitude = (decimal) 145.005345};
            var seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877852, Longtitude = (decimal) 145.005334};
            var list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var redZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Red};

            // Create a yellow geozone
            nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.876758, Longtitude = (decimal) 145.001949};
            swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877986, Longtitude = (decimal) 145.001471};
            neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877575, Longtitude = (decimal) 145.007404};
            seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.878587, Longtitude = (decimal) 145.007179};
            list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var yellowZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Yellow};

            // Create a green geozone 
            nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.874488, Longtitude = (decimal) 145.001776};
            swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.878339, Longtitude = (decimal) 144.997606};
            neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.875843, Longtitude = (decimal) 145.007740};
            seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.879522, Longtitude = (decimal) 145.007097};
            list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var greenZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Green};

            // Build a list of geozones for the school. 
            var schoolGeoZones = new List<SchoolGeoZone> {yellowZone, greenZone, redZone};

            var resultGeozone = GuardianService.DetectSchoolGeoZone(schoolGeoZones, (decimal) -37.877534,
                (decimal) 145.003022);

            Assert.Equal(resultGeozone, redZone);
        }

        /// <summary>
        /// The guardian is in a red zone that contains a red zone.
        /// 
        /// Expected: The DetectSchoolGeoZone method should return the correct geozone.
        /// </summary>
        [Fact]
        public void TestGuardianServiceRedzoneAroundRedzoneShouldReturnRedZone()
        {
            // For reference, the first red zone is the parking lot of Moorabbin Woolworths, the second red zone is the surrounding area. 
            // Create a red geozone
            var pointOne = new GeoZonePolygonPoint {Latitude = (decimal) -37.934070, Longtitude = (decimal) 145.038425};
            var pointTwo = new GeoZonePolygonPoint {Latitude = (decimal) -37.9394074, Longtitude = (decimal) 145.038489};
            var pointThree = new GeoZonePolygonPoint
            {
                Latitude = (decimal) -37.934188,
                Longtitude = (decimal) 145.039428
            };
            var pointFour = new GeoZonePolygonPoint {Latitude = (decimal) -37.934455, Longtitude = (decimal) 145.039347};
            var pointFive = new GeoZonePolygonPoint {Latitude = (decimal) -37.934370, Longtitude = (decimal) 145.038709};
            var pointSix = new GeoZonePolygonPoint {Latitude = (decimal) -37.935005, Longtitude = (decimal) 145.038564};
            var pointSeven = new GeoZonePolygonPoint
            {
                Latitude = (decimal) -37.949963,
                Longtitude = (decimal) 145.038275
            };
            var list = new List<GeoZonePolygonPoint>
            {
                pointOne,
                pointTwo,
                pointThree,
                pointFour,
                pointFive,
                pointSix,
                pointSeven
            };
            var redZone = new SchoolGeoZone
            {
                GeoZonePolygonPoints = list,
                GeoZoneTypeId = GeoZoneTypes.Red,
                Description = "First"
            };

            // Create a second red geozone 
            pointOne = new GeoZonePolygonPoint {Latitude = (decimal) -37.942045, Longtitude = (decimal) 145.027642};
            pointTwo = new GeoZonePolygonPoint {Latitude = (decimal) -37.944482, Longtitude = (decimal) 145.045667};
            pointThree = new GeoZonePolygonPoint {Latitude = (decimal) -37.929251, Longtitude = (decimal) 145.048585};
            pointFour = new GeoZonePolygonPoint {Latitude = (decimal) -37.927694, Longtitude = (decimal) 145.031161};
            list = new List<GeoZonePolygonPoint> {pointOne, pointTwo, pointThree, pointFour};
            var secondRedGeozone = new SchoolGeoZone
            {
                GeoZonePolygonPoints = list,
                GeoZoneTypeId = GeoZoneTypes.Red,
                Description = "Second"
            };

            // Build a list of geozones for the school. 
            var schoolGeoZones = new List<SchoolGeoZone> {redZone, secondRedGeozone};

            var resultGeozone = GuardianService.DetectSchoolGeoZone(schoolGeoZones, (decimal) -37.931195,
                (decimal) 145.039122);
            Assert.Equal(resultGeozone, secondRedGeozone);
        }

        /// <summary>
        /// The guardian is placed within a derated zone, which happens to be in an otherwise green zone. 
        /// 
        /// Expected: The GuardianService method returns the correct (derated) geozone. 
        /// </summary>
        [Fact]
        public void TestGuardianServiceDeratedZoneShouldReturnDeratedZone()
        {
            // Create a red geozone
            var nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877363, Longtitude = (decimal) 145.001928};
            var swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877441, Longtitude = (decimal) 145.001939};
            var neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877789, Longtitude = (decimal) 145.005345};
            var seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877852, Longtitude = (decimal) 145.005334};
            var list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var redZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Red};

            // Create a yellow geozone
            nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.876758, Longtitude = (decimal) 145.001949};
            swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877986, Longtitude = (decimal) 145.001471};
            neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877575, Longtitude = (decimal) 145.007404};
            seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.878587, Longtitude = (decimal) 145.007179};
            list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var yellowZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Yellow};

            // Create a green geozone 
            nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.874488, Longtitude = (decimal) 145.001776};
            swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.878339, Longtitude = (decimal) 144.997606};
            neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.875424, Longtitude = (decimal) 145.007794};
            seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.879522, Longtitude = (decimal) 145.007097};
            list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var greenZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Green};

            // Create a derated zone, make it within the green zone. For reference, this d-rated zone is Loch Avenue. 
            nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.875023, Longtitude = (decimal) 145.003087};
            swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.875149, Longtitude = (decimal) 145.003034};
            neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.875578, Longtitude = (decimal) 145.007760};
            seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.875769, Longtitude = (decimal) 145.007686};
            list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var deratedZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.DeRatedRed};

            // Build a list of geozones for the school. 
            var schoolGeoZones = new List<SchoolGeoZone> {yellowZone, greenZone, redZone, deratedZone};

            var resultGeozone = GuardianService.DetectSchoolGeoZone(schoolGeoZones, (decimal) -37.875108,
                (decimal) 145.003251);

            Assert.Equal(resultGeozone, deratedZone);
        }

        /// <summary>
        /// Tests first border case, the guardian is exactly on the border between green and yellow geozone. 
        /// 
        /// Expected: guardian is found to be in either the green or yellow zone (i.e. no unexpected error).
        /// </summary>
        [Fact]
        public void TestGuardianServiceBorderGreenYellowBorderCaseShouldReturnEitherZone()
        {
            // Create a red geozone
            var nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877363, Longtitude = (decimal) 145.001928};
            var swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877441, Longtitude = (decimal) 145.001939};
            var neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877789, Longtitude = (decimal) 145.005345};
            var seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877852, Longtitude = (decimal) 145.005334};
            var list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var redZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Red};

            // Create a yellow geozone, with a straight western border. 
            nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.876758, Longtitude = (decimal) 145.001949};
            swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877986, Longtitude = (decimal) 145.001949};
            neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877575, Longtitude = (decimal) 145.007404};
            seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.878587, Longtitude = (decimal) 145.007179};
            list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var yellowZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Yellow};

            // Create a green geozone 
            nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.874488, Longtitude = (decimal) 145.001776};
            swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.878339, Longtitude = (decimal) 144.997606};
            neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.875424, Longtitude = (decimal) 145.007794};
            seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.879522, Longtitude = (decimal) 145.007097};
            list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var greenZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Green};

            // Build a list of geozones for the school. 
            var schoolGeoZones = new List<SchoolGeoZone> {yellowZone, greenZone, redZone};

            var expectedList = new List<SchoolGeoZone> {greenZone, yellowZone};

            var resultGeozone = GuardianService.DetectSchoolGeoZone(schoolGeoZones, (decimal) -37.877534,
                (decimal) 145.001949);

            Assert.Contains(resultGeozone, expectedList);
        }

        /// <summary>
        /// Tests first border case, the guardian is exactly on the corner of the yellow geozone. 
        /// 
        /// Expected: guardian is found to be in either the green or yellow zone (i.e. no unexpected error).
        /// </summary>
        [Fact]
        public void TestGuardianServiceGreenYellowCornerCaseShouldReturnEitherZone()
        {
            // Create a red geozone
            var nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877363, Longtitude = (decimal) 145.001928};
            var swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877441, Longtitude = (decimal) 145.001939};
            var neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877789, Longtitude = (decimal) 145.005345};
            var seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877852, Longtitude = (decimal) 145.005334};
            var list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var redZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Red};

            // Create a yellow geozone, with a straight western border. 
            nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.876758, Longtitude = (decimal) 145.001949};
            swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877986, Longtitude = (decimal) 145.001949};
            neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.877575, Longtitude = (decimal) 145.007404};
            seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.878587, Longtitude = (decimal) 145.007179};
            list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var yellowZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Yellow};

            // Create a green geozone 
            nwCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.874488, Longtitude = (decimal) 145.001776};
            swCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.878339, Longtitude = (decimal) 144.997606};
            neCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.875424, Longtitude = (decimal) 145.007794};
            seCorner = new GeoZonePolygonPoint {Latitude = (decimal) -37.879522, Longtitude = (decimal) 145.007097};
            list = new List<GeoZonePolygonPoint> {swCorner, nwCorner, neCorner, seCorner};
            var greenZone = new SchoolGeoZone {GeoZonePolygonPoints = list, GeoZoneTypeId = GeoZoneTypes.Green};

            // Build a list of geozones for the school. 
            var schoolGeoZones = new List<SchoolGeoZone> {yellowZone, greenZone, redZone};

            var expectedList = new List<SchoolGeoZone> {greenZone, yellowZone};

            var resultGeozone = GuardianService.DetectSchoolGeoZone(schoolGeoZones, (decimal) -37.877986,
                (decimal) 145.001949);

            Assert.Contains(resultGeozone, expectedList);
        }
    }
}