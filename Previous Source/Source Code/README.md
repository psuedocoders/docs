# SchoolSmart Student Pickup Software #

## Repo Structure ##
Backend folder contains the ASP.NET MVC which contains the portals and ASP.NET Web API project which provides the end points for the apps and kiosks to communicate with the engine. 

Frontend folder contains the Android App which communicates with the backend through the Web API and push notifications.

Database folder contains a sql script that sets up the Postgres database.

Prototypes folder contains all of the spikes that were built to test various ideas. 

## How do I get set up? ##

1. Clone Repo
2. Build solution using Visual Studio
3. F5 to run MVC project

### Run Tests ###
XUnit is the testing framework using, with XUnit visual studio test runner currently referenced the tests can be run within Visual Studio.

### Android App ###
Android Studio is needed to build and run the Android App

### Deployment ###
Jenkins is setup on a Web24 VPS which automatically builds, tests and deploys the solution on every commit to the master branch.